package data

import (
	"net"
	"testing"
)

func TestCityLookup(t *testing.T) {
	var tests = []struct {
		input            *TemperatureSample
		expectedLocation string
		expectedError    error
	}{
		{
			input: &TemperatureSample{
				Temperature: 70,
				IPAddress:   net.ParseIP("1.1.1.1"),
			},
			expectedLocation: "blah",
			expectedError:    nil,
		},
	}

	for idx, test := range tests {
		t.Run(string(idx), func(t *testing.T) {
			loc, err := test.input.City()

			if err != test.expectedError {
				t.Fatalf("Expected %v but got %v", test.expectedError, err)
			}

			if loc != test.expectedLocation {
				t.Errorf("Expected %s but got %s", test.expectedLocation, loc)
			}
		})
	}
}
