package data

import (
	"fmt"
	"net"
	"strconv"
	"testing"
)

func TestExtractSampleFromLine(t *testing.T) {
	var (
		tests = []struct {
			input          string
			expectedSample *TemperatureSample
			expectedError  error
		}{
			{
				input: `2014-12-03 04:59:59	9804	22a99b5b-de0a-4f4f-aa78-5fb1e1a2910b	179E014C-7369-48E3-8C81-303DCFD3DCBD	189b0040-87f1-4e86-8b37-9164fd91c5df	0.61983468	qzvqehi	wuif5kke	hm7736z		zofj9nb4	728	90	0	TRUE	2001-01-01 00:00:00		4		d00r2s4	71	0x449EB5C1	1.08	97.68.237.19	{"tc":0.00024115701680000,"aid":"nx5xyrp","dgu":[{"deu":[{"id":2519868,"c":0.00008677685520000,"tpb":"zdkbnki","tpid":"34062","rrs":0.140000,}],"id":"eafchzf","rc":33,"rbs":0,"rbe":720},{"deu":[{"id":2519144,"c":0.00016115701680000,"tpb":"zdkbnki","tpid":"5952","rrs":0.260000,},{"id":2519521,"c":0.00016115701680000,"tpb":"zdkbnki","tpid":"30789","used":true,"rrs":0.260000,}],"id":"iine687","rc":19,"rbs":0,"rbe":720},{"deu":[{"id":2523842,"c":0.00008677685520000,"tpb":"zdkbnki","tpid":"5733","rrs":0.140000,}],"id":"mu3ej75","rc":39,"rbs":0,"rbe":720},{"deu":[{"id":-7,"c":0.000040,"tpb":"integral","tpid":"-7","used":true,"rcpm":0.040000}],"id":"charge-allIntegralBrandSafety",},{"deu":[{"id":-11,"c":0.000040,"tpb":"integral","tpid":"-11","used":true,"rcpm":0.040000}],"id":"charge-allIntegralSuspiciousActivity",}],"rc":19,"rbs":0,"rbe":720}	TRUE	64308		0.000011		en,es		{"10":0.00000100000,"2":0.000010000}		1	0		0.0008609916968	0.0009908996555	0.0009908996555	1	1	47549899	000000003c37162e	2	0		6s2idq3	{"10":{"AdjustmentOperationType":"ImpressionCountSamplingAdjustment","SamplingRateAttribute":"0.100000"}}`,
				expectedSample: &TemperatureSample{
					Temperature: 71,
					IPAddress:   net.ParseIP("97.68.237.19"),
				},
				expectedError: nil,
			},
			{
				input: `2014-12-03 05:00:00	5322	87512c04-8181-4d64-81a7-b098b1dee037	99f360ee105eeec11f7377879125ba5c658756f1	5daab386-085d-4b60-988b-32072a21b35a	0.55332928	9bny2ll	wibbtmhr	p3tigom		g1joylr5	160	600	0	TRUE	2001-01-01 00:00:00	26o	3	yt74sswr	6cwsd80		0x449EB5C1	1.12	208.54.70.159	{"tc":0.0,"aid":"whbn56d","dgu":[{"deu":[{"id":158224219,"c":0.0,"tpb":"none","tpid":"none","used":true,"rcpm":0.0}],"id":"rynwrhw","rc":13469,"rbs":10080,"rbe":20160}],"rc":13469,"rbs":10080,"rbe":20160}	TRUE	10452		0		en		{"10":0,"2":0}		1	0		0.00055332928	0.0006434061395	0.0006434061395	1	1	0000000043f33173	00000000291f7169	2	0			{"10":{"AdjustmentOperationType":"ImpressionCountSamplingAdjustment","SamplingRateAttribute":"0.780000"}}`,
				expectedSample: nil,
				expectedError:  TemperatureNotFoundErr,
			},
		}
	)

	for idx, test := range tests {
		t.Run(strconv.Itoa(idx), func(t *testing.T) {
			output, err := ExtractSample(test.input)
			if err != test.expectedError {
				t.Errorf("Expected %v but got %v", test.expectedError, err)
			}

			// Compare values by derefencing, rather than comparing memory addresses (pointers)
			if test.expectedSample != nil &&
				output != nil &&
				(output.Temperature != test.expectedSample.Temperature ||
					!output.IPAddress.Equal(test.expectedSample.IPAddress)) {
				t.Errorf("Expected %v but got %v", test.expectedSample, output)
			}
		})
	}

}

func TestProcessLine(t *testing.T) {
	const testLine = `2014-12-03 04:59:59	9804	22a99b5b-de0a-4f4f-aa78-5fb1e1a2910b	179E014C-7369-48E3-8C81-303DCFD3DCBD	189b0040-87f1-4e86-8b37-9164fd91c5df	0.61983468	qzvqehi	wuif5kke	hm7736z		zofj9nb4	728	90	0	TRUE	2001-01-01 00:00:00		4		d00r2s4	71	0x449EB5C1	1.08	97.68.237.19	{"tc":0.00024115701680000,"aid":"nx5xyrp","dgu":[{"deu":[{"id":2519868,"c":0.00008677685520000,"tpb":"zdkbnki","tpid":"34062","rrs":0.140000,}],"id":"eafchzf","rc":33,"rbs":0,"rbe":720},{"deu":[{"id":2519144,"c":0.00016115701680000,"tpb":"zdkbnki","tpid":"5952","rrs":0.260000,},{"id":2519521,"c":0.00016115701680000,"tpb":"zdkbnki","tpid":"30789","used":true,"rrs":0.260000,}],"id":"iine687","rc":19,"rbs":0,"rbe":720},{"deu":[{"id":2523842,"c":0.00008677685520000,"tpb":"zdkbnki","tpid":"5733","rrs":0.140000,}],"id":"mu3ej75","rc":39,"rbs":0,"rbe":720},{"deu":[{"id":-7,"c":0.000040,"tpb":"integral","tpid":"-7","used":true,"rcpm":0.040000}],"id":"charge-allIntegralBrandSafety",},{"deu":[{"id":-11,"c":0.000040,"tpb":"integral","tpid":"-11","used":true,"rcpm":0.040000}],"id":"charge-allIntegralSuspiciousActivity",}],"rc":19,"rbs":0,"rbe":720}	TRUE	64308		0.000011		en,es		{"10":0.00000100000,"2":0.000010000}		1	0		0.0008609916968	0.0009908996555	0.0009908996555	1	1	47549899	000000003c37162e	2	0		6s2idq3	{"10":{"AdjustmentOperationType":"ImpressionCountSamplingAdjustment","SamplingRateAttribute":"0.100000"}}`
	var expectedTokens = []string{
		"2014-12-03 04:59:59",
		"9804",
		"22a99b5b-de0a-4f4f-aa78-5fb1e1a2910b",
		"179E014C-7369-48E3-8C81-303DCFD3DCBD",
		"189b0040-87f1-4e86-8b37-9164fd91c5df",
		"0.61983468",
		"qzvqehi",
		"wuif5kke",
		"hm7736z",
		"",
		"zofj9nb4",
		"728",
		"90",
		"0",
		"TRUE",
		"2001-01-01 00:00:00",
		"",
		"4",
		"",
		"d00r2s4",
		"71",
		"0x449EB5C1",
		"1.08",
		"97.68.237.19",
		"{\"tc\":0.00024115701680000,\"aid\":\"nx5xyrp\",\"dgu\":[{\"deu\":[{\"id\":2519868,\"c\":0.00008677685520000,\"tpb\":\"zdkbnki\",\"tpid\":\"34062\",\"rrs\":0.140000,}],\"id\":\"eafchzf\",\"rc\":33,\"rbs\":0,\"rbe\":720},{\"deu\":[{\"id\":2519144,\"c\":0.00016115701680000,\"tpb\":\"zdkbnki\",\"tpid\":\"5952\",\"rrs\":0.260000,},{\"id\":2519521,\"c\":0.00016115701680000,\"tpb\":\"zdkbnki\",\"tpid\":\"30789\",\"used\":true,\"rrs\":0.260000,}],\"id\":\"iine687\",\"rc\":19,\"rbs\":0,\"rbe\":720},{\"deu\":[{\"id\":2523842,\"c\":0.00008677685520000,\"tpb\":\"zdkbnki\",\"tpid\":\"5733\",\"rrs\":0.140000,}],\"id\":\"mu3ej75\",\"rc\":39,\"rbs\":0,\"rbe\":720},{\"deu\":[{\"id\":-7,\"c\":0.000040,\"tpb\":\"integral\",\"tpid\":\"-7\",\"used\":true,\"rcpm\":0.040000}],\"id\":\"charge-allIntegralBrandSafety\",},{\"deu\":[{\"id\":-11,\"c\":0.000040,\"tpb\":\"integral\",\"tpid\":\"-11\",\"used\":true,\"rcpm\":0.040000}],\"id\":\"charge-allIntegralSuspiciousActivity\",}],\"rc\":19,\"rbs\":0,\"rbe\":720}",
		"TRUE",
		"64308",
		"",
		"0.000011",
		"",
		"en,es",
		"",
		"{\"10\":0.00000100000,\"2\":0.000010000}",
		"",
		"1",
		"0",
		"",
		"0.0008609916968",
		"0.0009908996555",
		"0.0009908996555",
		"1",
		"1",
		"47549899",
		"000000003c37162e",
		"2",
		"0",
		"",
		"6s2idq3",
		"{\"10\":{\"AdjustmentOperationType\":\"ImpressionCountSamplingAdjustment\",\"SamplingRateAttribute\":\"0.100000\"}}",
	}
	output, err := processLine(testLine)
	fmt.Printf("Num tokens %d\n", len(output))
	if err != nil {
		t.Errorf("Unexpected error %v", err)
		t.Errorf("Unexpected output %v", output)
		t.FailNow()
	}
	for idx, e := range output {
		if e != expectedTokens[idx] {
			t.Errorf("expected %v but got %v at position %d", expectedTokens[idx], e, idx)
		}
	}

	for idx, v := range output {
		fmt.Printf("%d %v\n", idx, v)
	}
}

func TestScanTabs(t *testing.T) {
	var (
		tests = []struct {
			name           string
			input          []byte
			expectedTokens []byte
		}{
			{
				name: "TabAfterByte",
				input: []byte(`a	`),
				expectedTokens: []byte("a"),
			},
			{
				name: "multipleTabs",
				input: []byte(`		a`),
				expectedTokens: []byte(""),
			},
		}
	)
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, output, err := scanTabs(test.input, true)
			if err != nil {
				t.Fatal(err)
			}
			if string(output) != string(test.expectedTokens) {
				t.Errorf("Expected %v but got %v", test.expectedTokens, output)
			}
		})
	}
}
