package data

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

var (
	TemperatureNotFoundErr = fmt.Errorf("Temperature value not found")
	IPAddressNotFoundErr   = fmt.Errorf("IP Address not found")
)

// TemperatureSample represents a single sample of a temperature (in Farenheit), and the location of the sample (IPAddress)
type TemperatureSample struct {
	Temperature float64
	IPAddress   net.IP
}

// ProcessFile reads a file at `path` and processes each line calling `ExtractSample`
func ProcessFile(path string) ([]*TemperatureSample, error) {
	var samples = []*TemperatureSample{}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(bufio.NewReader(f))
	scanner.Split(scanLines)
	for scanner.Scan() {
		sample, err := ExtractSample(scanner.Text())
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unexpected error \"%v\" parsing:\n \"%v\"\n\n", err, scanner.Text())
			continue
		}
		samples = append(samples, sample)
	}

	return samples, nil
}

// ExtractSample takes in the string data for a single sample, and returns a TemperatureSample
// extracted from the sample.
func ExtractSample(data string) (*TemperatureSample, error) {
	const (
		TemperatureIndex = 20
		IPAddressIndex   = 23
	)
	strings, err := processLine(data)
	if err != nil {
		return nil, err
	}

	sample := &TemperatureSample{}
	if strings[TemperatureIndex] == "" {
		return nil, TemperatureNotFoundErr
	}
	sample.Temperature, err = strconv.ParseFloat(strings[TemperatureIndex], 64)
	if err != nil {
		return nil, err
	}

	sample.IPAddress = net.ParseIP(strings[IPAddressIndex])
	if sample.IPAddress == nil {
		return nil, IPAddressNotFoundErr
	}

	return sample, nil
}

func scanTabs(data []byte, atEOF bool) (advance int, token []byte, err error) {
	return scanWords(data, atEOF, '\t')
}

func scanLines(data []byte, atEOF bool) (advance int, token []byte, err error) {
	return scanWords(data, atEOF, '\n')
}

func scanWords(data []byte, atEOF bool, delim rune) (advance int, token []byte, err error) {
	// Skip leading spaces.
	start := 0
	// for width := 0; start < len(data); start += width {
	// 	var r rune
	// 	r, width = utf8.DecodeRune(data[start:])
	// 	fmt.Printf("Got rune %v \n", r)
	// 	if r != '\t' {
	// 		break
	// 	}
	// }
	// Scan until space, marking end of word.
	for width, i := 0, start; i < len(data); i += width {
		var r rune
		r, width = utf8.DecodeRune(data[i:])
		if r == delim {
			return i + width, data[start:i], nil
		}
	}
	// If we're at EOF, we have a final, non-empty, non-terminated word. Return it.
	if atEOF && len(data) > start {
		return len(data), data[start:], nil
	}
	// Request more data.
	return start, nil, nil
}

func processLine(line string) ([]string, error) {
	const expectedTokensCount = 49
	scanner := bufio.NewScanner(strings.NewReader(line))
	count := 0
	output := []string{}
	scanner.Split(scanTabs)
	for scanner.Scan() {
		output = append(output, scanner.Text())
		count++
		if count > expectedTokensCount {
			return output, fmt.Errorf("line contains more tokens (%d) than expected (%d)", count, expectedTokensCount)
		}
	}
	return output, nil
}
