package kelvin

import (
	"fmt"
	"kelvin/pkg/kelvin/cmd"
	"os"
)

// Execute provides the trigger point for the root cmd
func Execute() {
	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
