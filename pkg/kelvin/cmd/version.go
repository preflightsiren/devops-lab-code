package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of Kelvin",
	Long:  `All software has versions. This is Kelvins's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Kelvin temperature grapher v0.0.0")
	},
}
