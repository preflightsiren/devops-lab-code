package cmd

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "kelvin",
	Short: "Kelvin is a CLI for analysing temperature data.",
	Long:  `Kelvin is a command line tool to generate histogram images, from a fictional temperature api.`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

// Execute runs the cobra command
func Execute() error {
	return rootCmd.Execute()
}
