package cmd

import (
	"fmt"
	histogram "github.com/VividCortex/gohistogram"
	"kelvin/pkg/data"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(printCmd)
}

var printCmd = &cobra.Command{
	Use:   "print",
	Short: "Print some of a data file",
	Run: func(cmd *cobra.Command, args []string) {
		samples, err := data.ProcessFile("histogram_input.csv")
		h := histogram.NewHistogram(80)

		if err != nil {
			fmt.Printf("%v", err)
			os.Exit(1)
		}
		for _, v := range samples {
			h.Add(v.Temperature)
		}
		fmt.Println(h)
		fmt.Printf("Mean: %0.0f\n", h.Mean())
		fmt.Printf("Var: %0.0f\n", h.Variance())
	},
}
