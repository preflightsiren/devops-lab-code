FROM golang:alpine as builder

WORKDIR /app

COPY . .

RUN go build -o kelvin ./cmd/kelvin

FROM alpine

COPY --from=builder /app/kelvin /

ENTRYPOINT ["/kelvin"]
CMD ["help"]
