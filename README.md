# DevOps-Lab-Code

This test is design to test out your creativity and enginuity! 

# Conditions

You have as long as you like to do this test (on average about a week should be enough) however it is expected that the resulting product will be what you consider to be production level.

# Tasks 

This repo contains a file called histogram_input.csv from this file you need to find the maximum temperature for tomorrow from each location listed in the file.  Pretend is is a log file being dumped out of a third party application so you have to work with what you have. 

To be sucessfull you need to do the following

1. Produce a CLI command that is re-runnable
2. Have the command log but skip over errors in the file
3. Produce a report of cities that are processed
4. Produce a CSV with a histogram of the temperature ranges
5. Include a dockerfile to build a container from your work
6. Please include help or a readme

For extra points consider the following

1. How to speed up any API calls

# Kelvin - the command line temperature tool

## Building

Kelvin is a golang application built using Make. To build Kelvin simply run the default make target

`make`

Other important targets can be shown from running `make help`

ex.
```
❯ make help

build                           Build the go binary
image                           Build a docker image to host Kelvin
test-image                      A test helper
clean                           Clean up on aisle 8
help                            This help target
```

## Code structure

The project structure is based off of https://github.com/golang-standards/project-layout

```
/cmd
    /kelvin - entrypoint for the app
/pkg
    /kelvin - Application setup and configuration
        /cmd - the commands that can be run
    /data - Things needed for processing `histogram_input.csv`
```