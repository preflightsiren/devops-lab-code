#!/bin/bash

docker run --rm -it \
                --name jupyter \
                -p 8888:8888 \
                -v $(pwd):/home/jovyan/work \
                --workdir /home/jovyan/work \
                jupyter/scipy-notebook:latest
