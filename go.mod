module kelvin

go 1.12

require (
	github.com/VividCortex/gohistogram v1.0.0
	github.com/spf13/cobra v0.0.5
)
