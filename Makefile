SHELL := $(shell which bash) 
.SHELLFLAGS = -c

.EXPORT_ALL_VARIABLES: ;	# send all vars to shell

kelvin: ## Build the go binary
	go build -o kelvin ./cmd/kelvin

image: ## Build a docker image to host Kelvin
	docker build -t kelvin .
.PHONY: image

test-image: ## A test helper
	touch error.log
	docker run --rm -ti \
	-v $(shell pwd)/histogram_input.csv:/histogram_input.csv \
	-v $(shell pwd)/error.log:/error.log \
	--entrypoint=/bin/sh \
	kelvin \
	"-c" "/kelvin print 2> /error.log"
.PHONY: test-image

clean: ## Clean up on aisle 8
	rm -f kelvin
	rm -f error.log
.PHONY: clean

# A help target including self-documenting targets (see the awk statement)
.PHONY: help
.SILENT: help
help: ## This help target
	echo "$$HELP_TEXT"
	awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / \
		{printf "\033[36m%-30s\033[0m  %s\n", $$1, $$2}' $(MAKEFILE_LIST)

